package view;

import java.time.LocalDateTime;
import java.util.Scanner;

import controller.Controller;
import model.data_structures.LinkedList;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;
import model.vo.Trip.Tipo;

public class View {

	public static void main(String[] args){

		Controller control = new Controller();
		Scanner linea = new Scanner(System.in);
		boolean fin = false; 
		while(!fin)
		{
			//Muestra cual fuente de datos va a cargar
			printMenu();

			int option = linea.nextInt();
			switch(option)
			{

			case 1:  //Carga de datos 

				//Memoria y tiempo
				long memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				long startTime = System.currentTimeMillis();

				Controller.cargarSistema(Manager.RUTA_JSON);

				//Tiempo en cargar
				long endTime = System.currentTimeMillis();
				long duration = endTime - startTime;

				//Memoria usada
				long memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				System.out.println("Tiempo en cargar: " + duration + " milisegundos \nMemoria utilizada:  "+ ((memoryAfterCase1 - memoryBeforeCase1)/1000000.0) + " MB");
				break;
				
			case 2:
				memoryBeforeCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				startTime = System.currentTimeMillis();
				
				Controller.sectorizacion();
				
				endTime = System.currentTimeMillis();
				duration = endTime - startTime;
				
				memoryAfterCase1 = Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory();
				break;
			case 3:
				System.out.println("Por favor ingrese una longitud.");
				double longitud = linea.nextDouble();
				System.out.println("Por favor ingrese una latitud");
				double latitud = linea.nextDouble();
				Controller.requerimiento1(longitud, latitud);
				break;
				
			case 4:
				System.out.println("Por favor ingrese una longitud.");
				longitud = linea.nextDouble();
				System.out.println("Por favor ingrese una latitud");
				latitud = linea.nextDouble();
				Controller.requerimiento2(longitud, latitud);
				break;
			case 5: //Salir
				fin = true;
				linea.close();
				break;
			}
		}
	}

	private static void printMenu()
	{
		System.out.println("---------ISIS 1206 - Estructuras de Datos----------");
		System.out.println("-------------------- Taller 5   - 2018-2 ----------------------");
		System.out.println("Iniciar la Fuente de Datos a Consultar :");
		System.out.println("1. Actualizar la informacion del sistema con las fuentes de datos CDOT_Bike_Routes");
		System.out.println("2. Realizar sectorización");
		System.out.println("3. REQUERIMIENTO 1");
		System.out.println("4. REQUERIMIENTO 2");
		System.out.println("5. salir");


	}


	/**
	 * Convertir fecha y hora a un objeto LocalDateTime
	 * @param fecha fecha en formato dd/mm/aaaa con dd para dia, mm para mes y aaaa para agno
	 * @param hora hora en formato hh:mm:ss con hh para hora, mm para minutos y ss para segundos
	 * @return objeto LDT con fecha y hora integrados
	 */
	private static LocalDateTime convertirFecha_Hora_LDT(String fecha, String hora)
	{
		String[] datosFecha = fecha.split("/");
		String[] datosHora = hora.split(":");

		int agno = Integer.parseInt(datosFecha[2]);
		int mes = Integer.parseInt(datosFecha[1]);
		int dia = Integer.parseInt(datosFecha[0]);
		int horas = Integer.parseInt(datosHora[0]);
		int minutos = Integer.parseInt(datosHora[1]);
		int segundos = Integer.parseInt(datosHora[2]);

		return LocalDateTime.of(agno, mes, dia, horas, minutos, segundos);
	}

}
