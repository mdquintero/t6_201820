package API;

import java.time.LocalDateTime;

import model.data_structures.LinkedList;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;

public interface IManager {
    
	/**
	 * Carga la informacion de las ciclorutas en el archivo JSON
	 * @param rutaTrips
	 */
	boolean cargarSistema(String direccionJson);

	
	LinkedList<Trip> getTrips();

}
