package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.SeparateChainingTH;


public class ChainingTHTest {

	
private SeparateChainingTH<Integer, Integer> chaining;
@Test
	public void test3()
	{
		long inicio = System.currentTimeMillis();
		chaining = new SeparateChainingTH<>(333333);
		for(int i = 0; i<1000000; i++)
		{
			int alfa = (int) Math.floor(Math.random()*3000000);
			int beta = (int) Math.floor(Math.random()*3000000);
			chaining.put(alfa, beta);
		}
		long fin = System.currentTimeMillis();
		long duracion = fin-inicio;
		System.out.println("Con un factor de 3 demor� " + duracion + " milisegundos en cargar");
		
		inicio = System.currentTimeMillis();
		for(int i=0; i<10000;i++)
		{
			int omega = (int) Math.floor(Math.random()*3000000);
			Integer theta = chaining.get(omega);
		}
		fin = System.currentTimeMillis();
		duracion = fin-inicio;
		System.out.println("Con un factor de 3 demor� " + duracion + " milisegundos en buscar 10000 datos");
	}
@Test
	public void test5()
	{
		long inicio = System.currentTimeMillis();
		chaining = new SeparateChainingTH<>(555555);
		for(int i = 0; i<1000000; i++)
		{
			int alfa = (int) Math.floor(Math.random()*3000000);
			int beta = (int) Math.floor(Math.random()*3000000);
			chaining.put(alfa, beta);
		}
		long fin = System.currentTimeMillis();
		long duracion = fin-inicio;
		System.out.println("Con un factor de 5 demor� " + duracion + " milisegundos en cargar");
		

		inicio = System.currentTimeMillis();
		for(int i=0; i<10000;i++)
		{
			int omega = (int) Math.floor(Math.random()*3000000);
			Integer theta = chaining.get(omega);
		}
		fin = System.currentTimeMillis();
		duracion = fin-inicio;
		System.out.println("Con un factor de 5 demor� " + duracion + " milisegundos en buscar 10000 datos");
	}
@Test
	public void test7()
	{
		long inicio = System.currentTimeMillis();
		chaining = new SeparateChainingTH<>(777777);
		for(int i = 0; i<1000000; i++)
		{
			int alfa = (int) Math.floor(Math.random()*3000000);
			int beta = (int) Math.floor(Math.random()*3000000);
			chaining.put(alfa, beta);
		}
		long fin = System.currentTimeMillis();
		long duracion = fin-inicio;
		System.out.println("Con un factor de 7 demor� " + duracion + " milisegundos en cargar");
		

		inicio = System.currentTimeMillis();
		for(int i=0; i<10000;i++)
		{
			int omega = (int) Math.floor(Math.random()*3000000);
			Integer theta = chaining.get(omega);
		}
		fin = System.currentTimeMillis();
		duracion = fin-inicio;
		System.out.println("Con un factor de 7 demor� " + duracion + " milisegundos en buscar 10000 datos");
	}

}
