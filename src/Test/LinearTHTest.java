package Test;

import static org.junit.Assert.*;

import org.junit.Test;

import model.data_structures.LinearProbingTH;

public class LinearTHTest {

	
	private LinearProbingTH<Integer, Integer> linear;
	@Test
	public void test025()
	{
		long inicio = System.currentTimeMillis();
		linear = new LinearProbingTH<>(4000000);
		for(int i = 0; i<1000000; i++)
		{
			int alfa = (int) Math.floor(Math.random()*3000000);
			int beta = (int) Math.floor(Math.random()*3000000);
			linear.put(alfa, beta);
		}
		long fin = System.currentTimeMillis();
		long duracion = fin-inicio;
		System.out.println("Con un factor de 0.25 demor� " + duracion + " milisegundos en cargar");
		
		inicio = System.currentTimeMillis();
		for(int i=0; i<10000;i++)
		{
			int omega = (int) Math.floor(Math.random()*3000000);
			Integer theta = linear.get(omega);
		}
		fin = System.currentTimeMillis();
		duracion = fin-inicio;
		System.out.println("Con un factor de 0.25 demor� " + duracion + " milisegundos en buscar 10000 datos");
	}
	@Test
	public void test05()
	{
		long inicio = System.currentTimeMillis();
		linear = new LinearProbingTH<>(2000000);
		for(int i = 0; i<1000000; i++)
		{
			int alfa = (int) Math.floor(Math.random()*3000000);
			int beta = (int) Math.floor(Math.random()*3000000);
			linear.put(alfa, beta);
		}
		long fin = System.currentTimeMillis();
		long duracion = fin-inicio;
		System.out.println("Con un factor de 0.5 demor� " + duracion + " milisegundos en cargar");
		

		inicio = System.currentTimeMillis();
		for(int i=0; i<10000;i++)
		{
			int omega = (int) Math.floor(Math.random()*3000000);
			Integer theta = linear.get(omega);
		}
		fin = System.currentTimeMillis();
		duracion = fin-inicio;
		System.out.println("Con un factor de 0.5 demor� " + duracion + " milisegundos en buscar 10000 datos");
	}
	@Test
	public void test075()
	{
		long inicio = System.currentTimeMillis();
		linear = new LinearProbingTH<>(1333333);
		for(int i = 0; i<1000000; i++)
		{
			int alfa = (int) Math.floor(Math.random()*3000000);
			int beta = (int) Math.floor(Math.random()*3000000);
			linear.put(alfa, beta);
		}
		long fin = System.currentTimeMillis();
		long duracion = fin-inicio;
		System.out.println("Con un factor de 0.75 demor� " + duracion + " milisegundos en cargar");
		

		inicio = System.currentTimeMillis();
		for(int i=0; i<10000;i++)
		{
			int omega = (int) Math.floor(Math.random()*3000000);
			Integer theta = linear.get(omega);
		}
		fin = System.currentTimeMillis();
		duracion = fin-inicio;
		System.out.println("Con un factor de 0.75 demor� " + duracion + " milisegundos en buscar 10000 datos");
	}

}
