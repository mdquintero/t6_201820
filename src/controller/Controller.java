package controller;

import java.time.LocalDateTime;

import API.IManager;
import model.data_structures.LinkedList;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.Station;
import model.vo.Trip;


public class Controller {
    private static Manager manager = new Manager();

  
    public static void cargarSistema(String dataJson){
    	manager.cargarSistema(dataJson);
    }
 
    public static void sectorizacion() {
    	manager.sectorizacion();
    }
    
    public static void requerimiento1(double longi, double lati)
    {
    	manager.requerimiento1(longi, lati);
    }
    public static void requerimiento2(double longi, double lati)
    {
    	manager.requerimiento2(longi, lati);
    }
    public int size()
    {
    	return manager.getTrips().size();
	}
  
  
}
