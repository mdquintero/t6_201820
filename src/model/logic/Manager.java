package model.logic;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.nio.file.FileSystemNotFoundException;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonIOException;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.google.gson.JsonSyntaxException;

import API.IManager;
import model.data_structures.LinearProbingTH;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.data_structures.SeparateChainingTH;
import model.data_structures.Stack;
import model.vo.Bike;
import model.vo.Route;
import model.vo.Sector;
import model.vo.Station;
import model.vo.Trip;

public class Manager implements IManager {


	public final static String rutaGeneral="."+File.separator+"data"+File.separator;
	//Ruta del archivo de datos 2017-Q1
	// TODO Actualizar
	public static final String RUTA_JSON = rutaGeneral+"CDOT_Bike_Routes_2014_1216-transformed(1).json";

	Sector[][] sectores = new Sector[10][10];

	private SeparateChainingTH<Integer,Sector> chaining = new SeparateChainingTH<Integer,Sector>(100); 

	private LinearProbingTH<Integer, Sector> linear = new LinearProbingTH<Integer,Sector>();

	private static LinkedList<Trip> linkTrips = new LinkedList<Trip>();

	private static LinkedList<Bike> listaBikes = new LinkedList<Bike>();

	private static LinkedList<Station> listaEncadenadaStations= new LinkedList<Station>();

	private Stack<Route> stackRoute = new Stack<Route>();

	private double longMinima = 0;

	private double longMax=0;

	private double latMinima=0;

	private double latMax=0;

	public LinkedList<Trip> getTrips()
	{
		return linkTrips;
	}

	public LinkedList<Bike> getBikes()
	{
		return listaBikes;
	}

	public LinkedList<Station> getStations()
	{
		return listaEncadenadaStations;
	}


	public void sectorizacion() {
		int contador = 1;
		for(int j = 0; j<10; j++) {

			for(int i = 0; i<10; i++) {

				//primer sector de toda la matriz
				if(i==0 && j==0)
				{
					sectores[i][j] = new Sector(longMinima, longMinima+((longMax-longMinima)/10), latMinima, latMinima+((latMax-latMinima)/10),contador);
					linear.put(contador, sectores[i][j]);
					chaining.put(contador, sectores[i][j]);
				}

				//primera fila
				else if(i!=0 && j==0) {
					sectores[i][j] = new Sector(sectores[i-1][0].longFinal()+0.00000000000001, ((longMax-longMinima)/10) + sectores[i-1][0].longFinal()+0.00000000000001, 
							sectores[0][0].latInicial(), sectores[0][0].latFinal(),contador );
					linear.put(contador, sectores[i][j]);
					chaining.put(contador, sectores[i][j]);
				}
				//primera columna
				else if(i==0 && j!=0) {
					sectores[i][j] = new Sector(sectores[0][0].longInicial(), sectores[0][0].longFinal(), 
							sectores[0][j-1].latFinal()+0.00000000000001, ((latMax-latMinima)/10) + (sectores[0][j-1].latFinal()+0.00000000000001),contador );
					linear.put(contador, sectores[i][j]);
					chaining.put(contador, sectores[i][j]);
				}
				//sector en i!=0 , j!=0
				else {
					sectores[i][j] = new Sector(sectores[i-1][0].longFinal()+0.00000000000001, ((longMax-longMinima)/10) + sectores[i-1][0].longFinal()+0.00000000000001,
							sectores[0][j-1].latFinal() + 0.00000000000001, ((latMax-latMinima)/10) + (sectores[0][j-1].latFinal() + 0.00000000000001),contador );
					linear.put(contador, sectores[i][j]);
					chaining.put(contador, sectores[i][j]);
				}
				System.out.println("LatInicial: " + sectores[i][j].latInicial() + ". LongInicial: " + sectores[i][j].longInicial());
				System.out.println("LatFinal: " + sectores[i][j].latFinal() + ". LongFinal: " + sectores[i][j].longFinal());
				System.out.println("ID: " + sectores[i][j].getID());
				contador++;
			}
		}
		
		sectorizarRutas();
	}

	public void sectorizarRutas()
	{
		Stack<Route> rutas = stackRoute;
		int tamano = rutas.size();
		for(int i=0;i<tamano;i++)
		{
			boolean iniciaEn=false;
			boolean terminaEn=false;
			Route actual = rutas.pop();
			double lat = actual.latInicial();
			double longi = actual.longInicial();
			Sector enSector = enSector(longi,lat);
			if(enSector!=null)
			{
				iniciaEn = !iniciaEn;
				enSector(longi,lat).addRuta(actual);
			}
			lat = actual.latFinal();
			longi = actual.longFinal();
			enSector = enSector(longi,lat);
			if(enSector!=null)
			{
				terminaEn = !terminaEn;
				if(!iniciaEn)
				{
					enSector(longi,lat).addRuta(actual);
				}
			}
			
			
		}
	}

	public void requerimiento1(double longi, double lati){
		Sector sectorID = enSector(longi,lati);
		if(sectorID!=null)
		{
			int ID = sectorID.getID();
			Sector sector = linear.get(ID);
			Route[] rutas = sector.getRutas();
			for(int i=0; i<rutas.length;i++)
			{
				if(rutas[i]!=null)
				{
					System.out.println("Sector: " + ID + " Street: " + rutas[i].getCalleReferencia() + " Longitud Inicial: " + rutas[i].longInicial() + " Latitud Inicial: " + rutas[i].latInicial());
				}
			}
		}else {
			System.out.println("Las coordenadas ingresadas no se encuentran en ningún sector");
		}
		
		
	}
	
	public void requerimiento2(double longi, double lati)
	{
		Sector sectorID = enSector(longi,lati);
		if(sectorID!=null)
		{
			int ID = sectorID.getID();
			System.out.println(ID);
			Sector sector = chaining.get(ID);
			System.out.println(sector.getID());
			if(sector !=null)
			{
				Route[] rutas = sector.getRutas();
				for(int i=0; i<rutas.length;i++)
				{
					if(rutas[i]!=null)
					{
						System.out.println("Sector: " + ID + " Street: " + rutas[i].getCalleReferencia() + " Longitud Inicial: " + rutas[i].longInicial() + " Latitud Inicial: " + rutas[i].latInicial());
					}
				}
			}
		}else {
			System.out.println("Las coordenadas ingresadas no se encuentran en ningún sector");
		}
		
		
	}
	public Sector enSector(double longi, double lati)
	{
		Sector r = null;
		for(int i=0;i<10;i++)
		{
			for(int j=0;j<10;j++)
			{
				Sector actual = sectores[i][j];
				double longInicial = actual.longInicial();
				double longFinal = actual.longFinal();
				double latInicial = actual.latInicial();
				double latFinal = actual.latFinal();
				if(longInicial<=longi && longFinal>=longi)
				{
					if(latInicial<=lati && latFinal>=lati)
					{
						r=actual;
					}
				}
			}
		}
		return r;
	}
	public double calcularDistancia(int inicio, int end)
	{
		boolean ini = false;
		boolean fini = false;
		Station referencia = null;
		Station ended = null;
		for(int i = 0; i<listaEncadenadaStations.size()&&(!ini||!fini); i++)
		{
			if(listaEncadenadaStations.get(i).getItem().getStationId()==inicio)
			{
				referencia = listaEncadenadaStations.get(i).getItem();
				ini = !ini;
			}
			if(listaEncadenadaStations.get(i).getItem().getStationId()==end)
			{
				ended = listaEncadenadaStations.get(i).getItem();
				fini = !fini;
			}
		}

		if(referencia != null && ended != null)
		{
			return calculateDistance(ended.getLat(), ended.getLongitude(), referencia.getLongitude(), referencia.getLat());
		}
		else {
			return 0;
		}
	}

	@Override 
	public boolean cargarSistema(String direccionJson) 
	{
		try
		{
			JsonParser parser = new JsonParser();
			JsonArray inicio = (JsonArray) parser.parse(new FileReader(direccionJson));
			JsonArray fin = inicio;


			for (int i = 0; fin != null && i < fin.size(); i++)

			{

				JsonObject obj = (JsonObject)fin.get(i);
				String id = "";
				String tipo ="";
				String ruta = "";
				String calleReferencia ="";
				double distancia = 0.0;
				String calleLimiteExtremo1 = "";
				String calleLimiteExtremo2 ="";	

				if(obj!=null)
				{
					if(obj.get("id")!=null)
						id=obj.get("id").getAsString();

					if(obj.get("BIKEROUTE")!=null)
						tipo = obj.get("BIKEROUTE").getAsString();

					if(obj.get("the_geom")!=null)
						ruta = obj.get("the_geom").getAsString();

					if(obj.get("STREET")!=null)
						calleReferencia = obj.get("STREET").getAsString();

					if(obj.get("Shape_leng")!=null)
						distancia = obj.get("Shape_leng").getAsDouble();

					if(obj.get("F_STREET")!=null && !obj.get("F_STREET").isJsonNull())
						calleLimiteExtremo1 = obj.get("F_STREET").getAsString();

					if(obj.get("T_STREET")!=null&& !obj.get("T_STREET").isJsonNull())
						calleLimiteExtremo2 = obj.get("T_STREET").getAsString();
				}

				ruta = ruta.replace("(", ",");
				ruta = ruta.replace(")", ",");
				System.out.println(ruta);
				String[] splits = ruta.split(",");
				Route t = new Route(id, tipo, splits, calleReferencia, calleLimiteExtremo1, calleLimiteExtremo2, distancia);
				for(int u = 1; u<splits.length;u++)
				{
					System.out.println(splits[u] + " ESTE ES EL SPLITS U");
					String[] coordenadas = splits[u].split(" ");
					System.out.println(coordenadas[0]  + "   " +"  "+ coordenadas[1]);
					double longi = 0.0;
					double lati = 0.0;
					if(coordenadas.length==3)
					{
						longi = Double.parseDouble(coordenadas[1]);
						lati = Double.parseDouble(coordenadas[2]);
					} else if (coordenadas.length==2)
					{
						longi = Double.parseDouble(coordenadas[0]);
						lati = Double.parseDouble(coordenadas[1]);
					}
					if(longMax==0)
					{
						longMax = longi;
					} else if(longi>longMax)
					{
						longMax = longi;
					}
					if(longMinima==0)
					{
						longMinima = longi;
					}else if(longi<longMinima)
					{
						longMinima = longi;
					}
					if(latMax==0)
					{
						latMax = lati;
					} else if(lati>latMax)
					{
						latMax = lati;
					}
					if(latMinima==0)
					{
						latMinima = lati;
					} else if(lati<latMinima)
					{
						latMinima = lati;
					}
				}

				stackRoute.push(t);
				System.out.println("Se ha a�adido la ruta numero " + (i+1));


			}
			System.out.println("Lat minima " + latMinima);
			System.out.println("lat Maxima " + latMax);
			System.out.println("Long minima " + longMinima);
			System.out.println("Long max " + longMax);
		}

		catch (JsonIOException e1 ) {
			e1.printStackTrace();
		}
		catch (JsonSyntaxException e2) {
			e2.printStackTrace();
		}
		catch (FileSystemNotFoundException e3) {
			e3.printStackTrace();
		} catch (FileNotFoundException e) {

			e.printStackTrace();
		}

		System.out.println("Inside loadServices with " + direccionJson);
		return false;
	}



	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas
	//METODO CREADO POR Maria Jose Ocampo Vargas

	public double calculateDistance (double lat1, double lon1, double longitudReferencia, double latitudReferencia) {

		final int R = 6371*1000; // Radious of the earth in meters 

		double latDistance = Math.toRadians(latitudReferencia-lat1); 

		double lonDistance = Math.toRadians(longitudReferencia-lon1); 

		double a = Math.sin(latDistance/2) * Math.sin(latDistance/2) + Math.cos(Math.toRadians(lat1))* Math.cos(Math.toRadians(latitudReferencia)) * Math.sin(lonDistance/2) * Math.sin(lonDistance/2); 

		double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a)); 

		double distance = R * c;

		return distance; 

	}

}
