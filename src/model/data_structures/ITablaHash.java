package model.data_structures;

import java.util.Iterator;

public interface ITablaHash<K, V> extends Iterable<K>{

	
	/**
	 *  * Agrega  una  dupla  (K,  V)  a  la  tabla.  Si  la  llave  K 
	 * existe,  se  reemplaza  su  valor  V  asociado.  V  no puede ser 
	 * null.
	 * @param key la llave del valor
	 * @param value el valor asociado a la llave
	 */
	void put(K key,V value);
	
	/**
	 * Se consigue el valor asociado a la llave K
	 * @return el valor asociado a la llave K
	 */
	V get(K key);
	
	/**
	 * Se borra la pareja asociada a la llave K
	 * @param key la llave de la dupla
	 * @return en caso de encontrar la llave retorna su valor V si no se encuentra la llave retorna null
	 */
	V delete(K key);

	
	
	
	
}
