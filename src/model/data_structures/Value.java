package model.data_structures;

public class Value {
	
	private Key llave;
	
	private Object objeto;
	
	public Value(Key llave, Object objeto)
	{
		this.llave = llave;
		this.objeto = objeto;
	}
}
