package model.data_structures;

public class Key implements Comparable<Key>{

	private Key next;
	
	private Value valor;
	
	public Key(Value valor)
	{
		this.valor = valor;
		this.next = null;
	}
	
	public Key(Key next, Value valor)
	{
		this.next = next;
		this.valor = valor;
	}

	@Override
	public int compareTo(Key o) {
		return 0;
	}

	public Key getNext() {
		
		return next;
	}
}
