package model.data_structures;

import java.util.Iterator;

public class ListIterator<K> implements Iterator<K> 
{
	private Key current;
	private Node<K> currentNode;
	public ListIterator() {
		// TODO Auto-generated constructor stub
	}
	public boolean hasNext()  { return current != null;                     }
	public void remove()      { throw new UnsupportedOperationException();  }

	public K next() {
		if (!hasNext()) return null;
		K item = (K) current;
		current = current.getNext(); 
		return item;
	}
	public void setCurrent(Key pCurrent) { current=pCurrent; }
	
	public void setCurrent(Node<K> frst) {currentNode= frst;}{
		// TODO Auto-generated method stub
		
	}
}